package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("/pizzas")
public class PizzaResource {
	public PizzaDao pizzas;
	
	@Context
    public UriInfo uriInfo;

    public PizzaResource() {
    	pizzas = BDDFactory.buildDao(PizzaDao.class);
    	pizzas.createTableAndIngredientAssociation();
    }

    @GET
    @Produces({ "application/json", "application/xml"})
    public List<PizzaDto> getAll() {
        
        List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
        return l;
    }
    
    @GET
    @Path("{id}")
    @Produces({ "application/json", "application/xml" })
    public PizzaDto getOnePizza(@PathParam("id") UUID id) {
    	try {
    		Pizza pizza = pizzas.findById(id);
    		return Pizza.toDto(pizza);
    	} catch(Exception e) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    }
    
    @GET
    @Path("{id}/name")
    @Produces(MediaType.TEXT_PLAIN)
    public String getPizzaName(@PathParam("id") UUID id) {
    	Pizza pizza = pizzas.findById(id);
    	
    	if(pizza == null) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    	
    	return pizza.getName();
    }
    
    @GET
    @Path("{id}/ingredients")
    @Produces({ "application/json", "application/xml" })
    public List<IngredientDto> getIngredients(@PathParam("id") UUID id) {
    	List<UUID> ingredientsIds = pizzas.getIngredientsIds(id);
    	List<IngredientDto> ingredients = new ArrayList<>();
    	IngredientDao daoIngredient = BDDFactory.buildDao(IngredientDao.class);
    	for(UUID i : ingredientsIds) {
    		Ingredient in = daoIngredient.findById(i);
    		ingredients.add(Ingredient.toDto(in));
    	}
    	
    	return ingredients;
    }
    
    @POST
    @Consumes({"application/json", "application/xml"})
    public Response createPizza(PizzaCreateDto pizzaCreateDto) {
    	Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
    	
    	if(existing != null) {
    		throw new WebApplicationException(Response.Status.CONFLICT);
    	}
    	
    	try {
    		Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
    		pizzas.insert(pizza);
    		List<Ingredient> ingredients = pizza.getIngredients();
    		List<UUID> idsIngredients = new ArrayList<>();
    		for(Ingredient i : ingredients) {
    			idsIngredients.add(i.getId());
    		}
    		for(UUID i : idsIngredients) {
    			pizzas.insertIngredients(pizza.getId(), i);
    		}
    		PizzaDto pizzaDto = Pizza.toDto(pizza);
    		
    		URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();
    		
    		return Response.created(uri).entity(pizzaDto).build();
    	} catch(Exception e) {
    		e.printStackTrace();
    		throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
    	}
    }

    @DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") UUID id) {
    	if(pizzas.findById(id) == null) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    	
    	pizzas.remove(id);
    	pizzas.removeIngredients(id);
    	
    	return Response.status(Response.Status.ACCEPTED).build();
    }
}

