package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PizzaDto {
	private UUID id;
	private String name;
	private List<UUID> ingredients;
	
	public PizzaDto() {}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<UUID> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<UUID> ingredients) {
		this.ingredients = ingredients;
	}
	
	

}
