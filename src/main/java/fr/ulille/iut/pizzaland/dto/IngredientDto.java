package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IngredientDto {
	private UUID id;
	private String name;
	
	public IngredientDto() {
        
    }

    public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
