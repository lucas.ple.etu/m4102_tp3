package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas(id varchar(128) PRIMARY KEY, "
    		+ "name VARCHAR NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation(idPizza varchar(128), "
    		+ "idIngredient varchar(128), "
    		+ "foreign key(idPizza) references Pizzas(id), "
    		+ "foreign key (idIngredient) references ingredients(id), "
    		+ "constraint pk_association primary key (idPizza, idIngredient))")
    void createAssociationTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropPizzaTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();

    @Transaction
    default void createTableAndIngredientAssociation() {
    	createPizzaTable();
    	createAssociationTable();
    }
    
    @Transaction
    default void dropTableAndIngredientAssociation() {
    	dropAssociationTable();
    	dropPizzaTable();
    }
    
    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();
    
    @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
	void insert(@BindBean Pizza pizza);
    
    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:idPizza, :idIngredient)")
	void insertIngredients(@Bind("idPizza") UUID idPizza, @Bind("idIngredient") UUID idIngredient);
    
    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);
    
    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(@Bind("id") UUID id);
    
    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
	void remove(@Bind("id") UUID id);
    
    @SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE idPizza = :id")
	void removeIngredients(@Bind("idPizza") UUID idPizza);
    
    @SqlQuery("SELECT * FROM PizzaIngredientsAssociation WHERE idPizza = :id")
    @RegisterBeanMapper(UUID.class)
    List<UUID> getIngredientsIds(@Bind("id") UUID id);
    
}