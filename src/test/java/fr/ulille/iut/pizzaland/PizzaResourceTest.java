package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
	private PizzaDao dao;
	private IngredientDao ingredientDao;

	@Context
	public UriInfo uriInfo;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		ingredientDao = BDDFactory.buildDao(IngredientDao.class);
		ingredientDao.createTable();
		dao.createTableAndIngredientAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAndIngredientAssociation();
		ingredientDao.dropTable();
	}

	@Test
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, pizzas.size());

	}

	@Test
	public void testGetExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Regina");
		dao.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);

	}
	
	@Test
    public void testGetNotExistingPizza() {
    	Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
    	assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
	
	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("Regina");
		dao.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Regina", response.readEntity(String.class));
	}
	
	@Test
	public void testGetPizzaIngredients() {
		Ingredient tomate = new Ingredient("tomate");
		ingredientDao.insert(tomate);
		Ingredient jambon = new Ingredient("jambon");
		ingredientDao.insert(jambon);
		List<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(tomate); ingredients.add(jambon);
		Pizza regina = new Pizza("Regina", ingredients);
		dao.insert(regina);
		for(Ingredient i : ingredients) {
			dao.insertIngredients(regina.getId(), i.getId());
		}
		
 		Response response = target("/pizzas").path(regina.getId().toString()).path("ingredients").request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		//assertEquals(ingredients, response.readEntity(List.class));
	}


	@Test
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Regina");

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	}

	@Test
    public void testCreateSamePizza() {
    	Pizza pizza = new Pizza();
        pizza.setName("Regina");
        dao.insert(pizza);

        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());

    }
    
    @Test
    public void testCreatePizzaWithoutName() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
    
	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("/pizzas/").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Regina");
		dao.insert(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = dao.findById(pizza.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas/").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
}
